<?php
// Croatian   v.2.x.x     Datum: 01.10.2014		Author: Gigo (Igor Ilić - igor@iligsoft.hr)
// Heading
$_['heading_title']        = 'Vaša narudžba je zaprimljena!';

// Text
$_['text_basket']          = 'Košarica';
$_['text_checkout']        = 'Naplata';
$_['text_success']         = 'Uspješno';
$_['text_customer']        = '<p>Vaša narudžba je uspješno zaprimljena i obrađena!</p><p>Možete pogledati pregled Vaših narudžbi na stranici <a href="%s">moj korisnički račun</a> ili kliknuvši na <a href="%s">povijest narudžbi</a>.</p>Za dodatne informacije o Vašoj narudžbi pošaljite upit na  <a href="mailto:webshop@kaonekad.hr">webshop@kaonekad.hr</a>.</p><p>Zahvaljujemo se što ste kupovali online kod nas!</p>';
$_['text_guest']           = '<p>Vaša narudžba je uspješno zaprimljena i obrađena!</p><p>Za dodatne informacije o Vašoj narudžbi pošaljite upit na  <a href="mailto:webshop@kaonekad.hr">webshop@kaonekad.hr</a>.</p><p>Zahvaljujemo se što ste kupovali online kod nas!</p>';