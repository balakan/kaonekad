<?php
// Heading

$_['heading_title']='Izdvojene kategorije';

// Tekst
$_['text_extension']='Moduli';
$_['text_success']='Uspjeh: Izmijenili ste Basel Content Builder';
$_['entry_name']='Ime modula';
$_['entry_status']='Status';
$_['button_save_stay']='Spremi i ostani';
$_['text_tab_content']='Graditelj sadržaja';
$_['text_tab_template']='Uvezi iz predloška';
$_['text_module_settings']='Postavke modula';
$_['text_block_settings']='Postavke područja bloka';
$_['text_use_block_title']='<span data-toggle="tooltip" title="Omogućite ga ako želite staviti naslov iznad bloka"> Naslov bloka </span>';
$_['text_block_pre_line']='<span data-toggle="tooltip" title="Neobvezno. Upotrijebite ovo polje za dodavanje bilo kojeg teksta iznad naslova."> Pred redak naslova </span>';
$_['text_block_title']='Naslov';
$_['text_block_sub_line']='<span data-toggle="tooltip" title="Neobvezno. Upotrijebite ovaj blok za dodavanje bilo kojeg teksta ispod naslova."> Pod redak naslova </span>';
$_['text_block_margin']='<span data-toggle="tooltip" title="Koristite ovo za poravnavanje bloka. Ako je onemogućeno, koristit će se zadane margine."> Prilagođene margine </span>';
$_['text_margin']='Margine';
$_['text_top']='Vrh';
$_['text_right']='U redu';
$_['text_bottom']='Dno';
$_['text_left']='Lijevo';
$_['text_full_width_background']='<span data-toggle="tooltip" title="Ne koristite blokove pune širine unutar položaja Sadržaj gore / Sadržaj dno"> Blok pune širine </span>';
$_['text_use_background_color']='Boja pozadine';
$_['text_background_color']='Boja';
$_['text_use_background_image']='Pozadinska slika';
$_['text_background_image']='Slika';

$_['text_background_position']='<span data-toggle="tooltip" title="Ako je Parallax pomicanje omogućeno, položaj pozadine uvijek će biti gornji centar"> Pozadinski položaj </span>';
$_['text_background_repeat']='Ponavljanje pozadine';
$_['text_use_background_video']='Video u pozadini';
$_['text_background_video']='<span data-toggle="tooltip" title="Na primjer, ako je video youtube.com/watch?v=VH7__ZLzUGw, ID je VH7__ZLzUGw"> Youtube video ID </ span > ';
$_['text_use_css']='Prilagođeni CSS';
$_['text_css']='CSS';


$_['text_content_settings']='Postavke područja sadržaja';
$_['text_full_width_content']='<span data-toggle="tooltip" title="Postavlja područje sadržaja na punu širinu. Zahtijeva da i blok bude postavljen na punu širinu."> Sadržaj pune širine </span> ';
$_['text_zero_margin']='<span data-toggle="tooltip" title="Ako je omogućeno, neće se koristiti prostor za odvajanje stupaca"> Bez margina </span>';
$_['text_equal_height']='<span data-toggle="tooltip" title="Ako je omogućeno, svaki stupac sadržaja imat će istu visinu"> Stupci jednake visine </span>';
$_['text_content_columns']='Stupci';
$_['text_column']='Stupac';
$_['text_add_column']='Dodaj stupac';
$_['text_column_width']='<span data-toggle="tooltip" title="12/12 znači 100% širine, 6/12 znači 50% širine itd."> Širina stupca </span>';
$_['text_width_per_device']='Širina';
$_['text_type']='Vrsta sadržaja';
$_['text_select_type']='Odabir vrste sadržaja';
$_['text_title_html']='HTML';
$_['text_position']='<span data-toggle="tooltip" title="(Okomito / vodoravno) - Primijenit će se okomito poravnanje ako je stupac veći od njegovog sadržaja"> Poravnanje sadržaja </span>';
$_['text_html_content']='Sadržaj';
$_['text_enable_editor']='Omogući HTML editor';
$_['text_disable_editor']='Onemogući HTML editor';
$_['text_view_icons']='Prikaži vektorske ikone';
$_['text_view_shortcodes']='Prikaži dostupne kratke kodove';
$_['text_view_overlays']='Prikaži prekrivene kratke kodove';
$_['text_html']='HTML';
$_['text_banner']='Natpis';
$_['text_title_banner']='Natpis';
$_['text_title_banner2']='Natpis 2';
$_['text_link_target']='<span data-toggle="tooltip" title="Uključi http: // prilikom povezivanja na vanjske stranice"> URL (link) </span>';
$_['text_banner_overlay']='Prekrivanje';
$_['text_position_banner']='<span data-toggle="tooltip" title="(Vertical / Horizontal)"> Prekrivajući položaj </span>';
$_['text_btn_add_banner']='Dodaj drugi natpis unutar istog stupca';
$_['text_remove_banner']='Ukloni natpis';
$_['text_overlay_position']='<span data-toggle="tooltip" title="(Okomito / vodoravno) - Primijenit će se okomito poravnanje ako je stupac veći od njegovog sadržaja"> Poravnanje sadržaja </span>';
$_['text_testimonial']='Svjedočanstva';
$_['text_title_testimonial']='Karusel za svjedočenja';
$_['text_limit']='<span data-toggle="tooltip" title="Broj slučajnih svjedočanstava za prikaz"> Ograničenje </span>';
$_['text_tm_columns']='Stupci';
$_['text_tm_style']='Stil';
$_['text_tm_style_plain']='Obično';
$_['text_tm_style_plain_light']='Običan (lagani tekst)';
$_['text_tm_style_block']='Kutije s stilom materijala';
$_['text_template']='Naziv predloška';
$_['text_action']='Akcija';
$_['text_preview']='Pregled';
$_['text_import']='Uvezi';
$_['text_confirm']='Svi trenutni podaci bit će izgubljeni. Želite li nastaviti?';
$_['text_icons_list']='Popis ikona';
$_['text_preview_template']='Pregled';
$_['text_set_width_per_device']='Postavi širinu po uređaju';
$_['text_hidden']='Skriveno';
$_['text_columns_settings']='Postavke stupca';
$_['text_banner_help']='<b> Kratke šifre natpisa: </b> (Dodaj raspone u blok prekrivanja) <br /> & lt; span class=& quot; hover-zoom & quot; & gt; & lt; / span & gt;=Zumiranje slike na hover <br /> & lt; span class=& quot; hover-darken & quot; & gt; & lt; / span & gt;=Dodaje tamni sloj na sliku na hover <br /> & lt; span class=& quot; hover-border & quot; & gt; & lt; / span & gt;=Dodaje animirani efekt obruba na hover <br /> & lt; span class=& quot; hover-up & quot; & gt; & lt; / span & gt;=Premješta sliku prema gore prema kursu <br /> & lt; span class=& quot; lebdi prema dolje & quot; & gt; & lt; / span & gt;=Premješta sliku prema dolje lebdenjem <br /> & lt; div class=& quot; hidden-overlay & quot; & gt; ... & lt; / div & gt;=Skriva njegov sadržaj dok ne pređe lebdenje ';
$_['text_layout_example']='Primjer izgleda';
$_['text_page']='Preglednik';
$_['text_block']='Blok područje';
$_['text_content']='Područje sadržaja';

// Pogreška
$_['error_permission']='Upozorenje: Nemate dopuštenje za izmjenu Basel Content Builder-a';
$_['error_name']='Ime modula mora biti između 3 i 64 znaka!';


