<?php
// Heading
$_['heading_title']      = 'Grupe proizvoda';

$_['text_info']    		 = '
<h4 style="margin-bottom:10px;">Product Groups</h4>
<p>The product groups feature can be used to display selected products in your store front. Groups are defined in the form below.</p>
<p>Product groups configured in the form below can be used when creating a <i>Basel Products</i> module.</p>';
$_['text_to_modules'] = 'Idi na module';
// Tekst
$_['text_success'] = 'Uspjeh: Izmijenili ste grupe proizvoda';
$_['text_enable'] = 'Omogući';
$_['text_disable'] = 'Onemogući';
$_['text_module'] = 'Modul';
$_['text_tab'] = 'Grupa';
$_['text_notitle'] = '(bez naslova)';
$_['text_best_seller'] = 'Najprodavaniji';
$_['text_latest_products'] = 'Najnoviji proizvodi';
$_['text_special_products'] = 'Akcijski proizvodi';
$_['text_popular_products'] = 'Popularni proizvodi';
$_['text_all_categories'] = 'Sve kategorije';
$_['text_all_manufacturer'] = 'Svi proizvođači';
$_['text_sort_name'] = 'Po imenu';
$_['text_sort_rating'] = 'Po ocjeni (prvo najviša)';
$_['text_sort_sort_order'] = 'Po redoslijedu sortiranja';
$_['text_sort_price'] = 'Po cijeni (prvo najniža)';
$_['text_sort_added'] = 'Po datumu dodavanja (prvo najnovije)';
$_['text_category'] = 'Kategorija';
$_['text_manufacturer'] = 'Proizvođač';

// Ulaz
$_['entry_title'] = 'Naziv grupe';
$_['entry_products'] = 'Proizvodi';
$_['entry_filter'] = 'Filtri: <span class="help" title="Odaberite proizvode za filtriranje prema kategoriji i / ili proizvođaču."> </span>';
$_['entry_sort_query'] = 'Poredaj po';
$_['entry_group'] = 'Grupa';
$_['entry_source'] = 'Proizvodi';
$_['entry_limit'] = 'Ograničenje: <span class="help" title="Najviše proizvoda za prikaz u ovom modulu (u svakoj grupi)."> [?] </span>';

// Gumbi
$_['button_add_tab'] = 'Dodaj grupu';
$_['button_remove'] = 'Ukloni';

// zaglavlja
$_['header_tabs'] = 'Konfiguracija grupe';
$_['header_products_select'] = 'Odabir proizvoda pojedinačno';
$_['header_predefined_groups'] = 'Odaberite iz zadanih grupa Opencarts';
$_['header_custom_query'] = 'Odabir prema kategoriji i proizvođaču';

// Pogreška
$_['error_permission'] = 'Upozorenje: Nemate dopuštenje za izmjenu grupa proizvoda ';

