<?php
class ControllerStartupPermission extends Controller {
	public function index() {
		if (isset($this->request->get['route'])) {
			$route = '';

			$part = explode('/', $this->request->get['route']);

            if (isset($part[3]) &&
                $part[3] == 'importCategories' ||
                $part[3] == 'importManufacturers' ||
                $part[3] == 'importProducts' ||
                $part[3] == 'importActions' ||
                $part[3] == 'checkMinQty' ||
                $part[3] == 'checkMinQtyOfCategories' ||
                $part[3] == 'updatePrices' ||
                $part[3] == 'updateQuantities') {
                return;
            }

			if (isset($part[0])) {
				$route .= $part[0];
			}

			if (isset($part[1])) {
				$route .= '/' . $part[1];
			}

			// If a 3rd part is found we need to check if its under one of the extension folders.
			$extension = array(
				'extension/advertise',
				'extension/dashboard',
				'extension/analytics',
				'extension/captcha',
				'extension/extension',
				'extension/feed',
				'extension/fraud',
				'extension/module',
				'extension/payment',
				'extension/shipping',
				'extension/theme',
				'extension/total',
				'extension/report'
			);

			if (isset($part[2]) && in_array($route, $extension)) {
				$route .= '/' . $part[2];
			}

			// We want to ingore some pages from having its permission checked.
			$ignore = array(
				'common/dashboard',
				'common/login',
				'common/logout',
				'common/forgotten',
				'common/reset',
				'error/not_found',
				'error/permission',
                'extension/module/luceed_sync/importCategories',
                'extension/module/luceed_sync/importManufacturers',
                'extension/module/luceed_sync/importProducts',
                'extension/module/luceed_sync/importActions',
                'extension/module/luceed_sync/checkMinQty',
                'extension/module/luceed_sync/checkMinQtyOfCategories',
                'extension/module/luceed_sync/updatePrices',
                'extension/module/luceed_sync/updateQuantities',
			);

			if (!in_array($route, $ignore) && !$this->user->hasPermission('access', $route)) {
				return new Action('error/permission');
			}
		}
	}
}
